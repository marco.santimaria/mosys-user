#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

int main(int argc, char **argv) {
    const char *file = "/dev/flash0,1";

    int fd = open(file, O_RDONLY);
    if (fd < 0) {
        err(EXIT_FAILURE, "Failed to open %s", file);
    }

    uint32_t version;
    pread(fd, &version, sizeof(version), 0x400);
    if (fd < 0) {
        err(EXIT_FAILURE, "Failed to read device");
    }

    uint16_t rom_release_version = version & 0xFFFF;
    uint16_t rom_release_flags = version >> 16;

    printf("System ROM version %2hhx.%02hhx%s%s%s\n",
        (unsigned char) (rom_release_version >> 8), (unsigned char) (rom_release_version >> 0),
        (rom_release_flags & (1 << 15U)) ? ".DEV" : "",
        (rom_release_flags & (1 << 14U)) ? ", requires large SDB" : "",
        (rom_release_flags & (1 << 13U)) ? ", flashable" : "");
}
