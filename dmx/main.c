#include "font8x8/font8x8_latin.h"
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <asm/ioctls.h>
#include <sys/ioctl.h>
#include <mosys/v99x8.h>

char screen_buf[212][256];

typedef struct pixel {
    uint8_t colour;
} pixel;

typedef struct point {
    uint16_t x;
    uint16_t y;
} point;

typedef struct size {
    uint16_t x;
    uint16_t y;
} size;

typedef struct offset {
    int16_t x;
    int16_t y;
} offset;

typedef struct region {
    point pos;
    size sz;
} region;

typedef struct window_contents {
    pixel background;
} window_contents;

typedef struct window {
    region reg;

    window_contents contents;
    pixel *data;

    struct window *next;
} window;

typedef struct session {
    size sz;

    window *root_wind;
} session;

window *window_new(session *sesh, region reg, pixel background);
void window_append_session(session *sesh, window *wind);
pixel *window_data_new(size sz);
void window_putchar(window *wind, char c, point pos, pixel fg);
void window_puts(window *wind, const char *s, point pos, pixel fg);

bool window_resize_data(window *wind, size sz);
bool window_move_resize(window *wind, offset delta_pos, offset delta_sz);

void fill_background(pixel *data, size sz, pixel background);

void draw_session(int fd, session *sesh);
void draw_session_masked(int fd, session *sesh, region mask);
bool draw_session_until(int fd, session *sesh, window *until);
bool draw_session_masked_until(int fd, session *sesh, window *until, region mask);

void draw_window(int fd, session *sesh, window *wind);
void draw_window_masked(int fd, session *sesh, window *wind, region mask);
bool draw_window_until(int fd, session *sesh, window *wind, window *until);
bool draw_window_masked_until(int fd, session *sesh, window *wind, window *until, region mask);
void draw_under_window(int fd, session *sesh, window *wind);
void draw_window_raw(int fd, window *wind, region mask);

session *session_new(size sz);

window *window_new(session *sesh, region reg, pixel background) {
    window *wind = malloc(sizeof(window));
    if (!wind) {
        return NULL;
    }

    wind->reg = reg;

    wind->contents.background = background;

    wind->data = window_data_new(reg.sz);
    if (!wind->data) {
        free(wind);
        return NULL;
    }

    wind->next = NULL;
    window_append_session(sesh, wind);

    return wind;
}

pixel *window_data_new(size sz) {
    size_t size = sz.y * sz.x * sizeof(pixel);

    pixel *data = malloc(size);
    if (!data) {
        return NULL;
    }

    memset(data, 0, size);
    return data;
}

void window_append_session(session *sesh, window *wind) {
    window **ptr = &sesh->root_wind;
    while (*ptr) {
        ptr = &(*ptr)->next;
    }
    *ptr = wind;
}

void window_putchar(window *wind, char c, point pos, pixel fg) {
    char *c_font;
    if (c < 0x80) {
        c_font = font8x8_basic[c - 0x00];
    } else if (c < 0xA0) {
        c_font = font8x8_control[c - 0x80];
    } else {
        c_font = font8x8_ext_latin[c - 0xA0];
    }

    for (unsigned short pix_y = 0; pix_y < 8; ++pix_y) {
        for (unsigned short pix_x = 0; pix_x < 8; ++pix_x) {
            if ((c_font[pix_y] >> pix_x) & 0x01) {
                unsigned short pix_pos_x = pos.x + pix_x;
                unsigned short pix_pos_y = pos.y + pix_y;
                unsigned short pix_offset = pix_pos_y * wind->reg.sz.x + pix_pos_x;
                wind->data[pix_offset] = fg;
            }
        }
    }
}

void window_puts(window *wind, const char *s, point pos, pixel fg) {
    for (; *s; ++s) {
        switch (*s) {
        case '\n':
            pos.x = 0;
            pos.y += 8;
            break;
        default:
            window_putchar(wind, *s, pos, fg);
            pos.x += 8;
            break;
        }
    }
}

bool window_resize_data(window *wind, size sz) {
    pixel *new_data = window_data_new(sz);
    if (!new_data) {
        return false;
    }

    fill_background(new_data, sz, wind->contents.background);

    size_t copy_x = sz.x < wind->reg.sz.x ? sz.x : wind->reg.sz.x;
    size_t copy_y = sz.y < wind->reg.sz.y ? sz.y : wind->reg.sz.y;
    for (unsigned short y = 0; y < copy_y; ++y) {
        memmove(new_data + y * sz.x, wind->data + y * wind->reg.sz.x, copy_x * sizeof(pixel));
    }

    free(wind->data);
    wind->data = NULL;

    wind->reg.sz.x = sz.x;
    wind->reg.sz.y = sz.y;
    wind->data = new_data;

    return true;
}

bool window_move_resize(window *wind, offset delta_pos, offset delta_sz) {
    bool result = true;

    if (delta_pos.x < 0 && wind->reg.pos.x <= -delta_pos.x) {
        wind->reg.pos.x = 0;
    } else {
        wind->reg.pos.x = (int) wind->reg.pos.x + (int) delta_pos.x;
    }

    if (delta_pos.y < 0 && wind->reg.pos.y <= -delta_pos.y) {
        wind->reg.pos.y = 0;
    } else {
        wind->reg.pos.y = (int) wind->reg.pos.y + (int) delta_pos.y;
    }

    if (delta_sz.x || delta_sz.y) {
        size new_sz;

        if (delta_sz.x < 0 && wind->reg.sz.x <= -delta_sz.x) {
            new_sz.x = 0;
        } else {
            new_sz.x = (int) wind->reg.sz.x + (int) delta_sz.x;
        }

        if (delta_sz.y < 0 && wind->reg.sz.y <= -delta_sz.y) {
            new_sz.y = 0;
        } else {
            new_sz.y = (int) wind->reg.sz.y + (int) delta_sz.y;
        }

        result = window_resize_data(wind, new_sz);
    }

    return result;
}

void fill_background(pixel *data, size sz, pixel background) {
    const size_t sz_pix = sz.y * sz.x;
    for (size_t i = 0; i < sz_pix; ++i) {
        data[i] = background;
    }
}

void draw_session(int fd, session *sesh) {
    draw_session_until(fd, sesh, NULL);
}

void draw_session_masked(int fd, session *sesh, region mask) {
    draw_session_masked_until(fd, sesh, NULL, mask);
}

bool draw_session_until(int fd, session *sesh, window *until) {
    return draw_window_masked_until(fd, sesh, sesh->root_wind, until, (region) {{0, 0}, sesh->sz});
}

bool draw_session_masked_until(int fd, session *sesh, window *until, region mask) {
    return draw_window_masked_until(fd, sesh, sesh->root_wind, until, mask);
}

void draw_window(int fd, session *sesh, window *wind) {
    draw_window_until(fd, sesh, wind, NULL);
}

void draw_window_masked(int fd, session *sesh, window *wind, region mask) {
    draw_window_masked_until(fd, sesh, wind, NULL, mask);
}

bool draw_window_until(int fd, session *sesh, window *wind, window *until) {
    return draw_window_masked_until(fd, sesh, wind, until, wind->reg);
}

bool draw_window_masked_until(int fd, session *sesh, window *wind, window *until, region mask) {
    while (wind && wind != until) {
        draw_window_raw(fd, wind, mask);
        wind = wind->next;
    };
    return wind == until;
}

void draw_under_window(int fd, session *sesh, window *wind) {
    draw_session_masked_until(fd, sesh, wind, wind->reg);
}

void draw_window_raw(int fd, window *wind, region mask) {
    // Return early on zero-size window
    if (wind->reg.sz.x == 0 || wind->reg.sz.y == 0) {
        return;
    }

    struct v99x8_ioc_command ioc_command = {
        0, 0,
        wind->reg.pos.x, wind->reg.pos.y,
        wind->reg.sz.x, wind->reg.sz.y,
        0,
        0,
        0,
        (void *) wind->data,
        NULL,

        -1,
        0
    };

    if (ioctl(fd, V99X8_IOC_LMMC, &ioc_command) < 0) {
        err(EXIT_FAILURE, "Failed to write");
    }
}

session *session_new(size sz) {
    session *sesh = calloc(1, sizeof(session));
    sesh->sz.x = sz.x;
    sesh->sz.y = sz.y;
    sesh->root_wind = NULL;
    return sesh;
}

int main(int argc, char **argv) {
    const char *fb_name = "/dev/fb0";

    int fd = open(fb_name, O_RDWR);
    if (fd < 0) {
        err(EXIT_FAILURE, "Failed to open %s", fb_name);
    }

    for (int i = 0; i < 212; ++i) {
        for (int j = 0; j < 256; ++j) {
            screen_buf[i][j] = i + j;
        }
    }

    session *sesh = session_new((size) {256, 212});

    window *wind0 = window_new(sesh, (region) { { 0, 0 }, { 256, 212 } }, (pixel) { 0x00 });
    window *wind1 = window_new(sesh, (region) { { 70, 70 }, { 70, 35 } }, (pixel) { 0xF0 });
    window *wind2 = window_new(sesh, (region) { { 50, 50 }, { 70, 35 } }, (pixel) { 0x0F });

    if (!wind0 || !wind1 || !wind2) {
        err(EXIT_FAILURE, "Failed to allocate windows");
    }

    fill_background(wind0->data, wind0->reg.sz, wind0->contents.background);
    fill_background(wind1->data, wind1->reg.sz, wind1->contents.background);
    fill_background(wind2->data, wind2->reg.sz, wind2->contents.background);

    window_puts(wind1, "Hello\nWorld", (point) { 0, 0 }, (pixel) { 0xFF });

    draw_session(fd, sesh);

    struct termios termios_orig;
    ioctl(STDIN_FILENO, TCGETS, &termios_orig);

    struct termios termios_new = termios_orig;
    termios_new.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP |
                             INLCR | IGNCR | ICRNL | IXON);
    termios_new.c_oflag &= ~OPOST;
    termios_new.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    termios_new.c_cflag &= ~(CSIZE | PARENB);
    termios_new.c_cflag |= CS8;
    ioctl(STDIN_FILENO, TCSETS, &termios_new);

    bool keep_looping = true;
    while (keep_looping) {
        char c_in;
        if (read(STDIN_FILENO, &c_in, 1) == 1) {
            window *active_wind = wind1;

            offset delta_pos = { 0, 0 };
            offset delta_sz = { 0, 0 };

            switch (c_in) {
            case 'w':
                delta_pos.y = -1;
                break;
            case 'W':
                delta_pos.y = -10;
                break;
            case 'W' - '@': // '^W'
                delta_pos.y = -50;
                break;

            case 'a':
                delta_pos.x = -1;
                break;
            case 'A':
                delta_pos.x = -10;
                break;
            case 'A' - '@': // '^A'
                delta_pos.x = -50;
                break;

            case 's':
                delta_pos.y = 1;
                break;
            case 'S':
                delta_pos.y = 10;
                break;
            case 'S' - '@': // '^S'
                delta_pos.y = 50;
                break;

            case 'd':
                delta_pos.x = 1;
                break;
            case 'D':
                delta_pos.x = 10;
                break;
            case 'D' - '@': // '^D'
                delta_pos.x = 50;
                break;

            case 'i':
                delta_sz.y = -1;
                break;
            case 'I':
                delta_sz.y = -10;
                break;
            case 'I' - '@': // '^I'
                delta_sz.y = -50;
                break;

            case 'j':
                delta_sz.x = -1;
                break;
            case 'J':
                delta_sz.x = -10;
                break;
            case 'J' - '@': // '^J'
                delta_sz.x = -50;
                break;

            case 'k':
                delta_sz.y = 1;
                break;
            case 'K':
                delta_sz.y = 10;
                break;
            case 'K' - '@': // '^K'
                delta_sz.y = 50;
                break;

            case 'l':
                delta_sz.x = 1;
                break;
            case 'L':
                delta_sz.x = 10;
                break;
            case 'L' - '@': // '^L'
                delta_sz.x = 50;
                break;

            case 'q':
            case 'Q':
                keep_looping = false;
                break;

            default:
                break;
            }

            bool need_update = delta_pos.x || delta_pos.y || delta_sz.x || delta_sz.y;

            if (need_update) {
                draw_under_window(fd, sesh, active_wind);
                window_move_resize(active_wind, delta_pos, delta_sz);
                draw_window(fd, sesh, active_wind);
            }
        }
    }

    ioctl(STDIN_FILENO, TCSETS, &termios_orig);
}
