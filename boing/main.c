#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <mosys/v99x8.h>
#include "util.h"
#include "v99x8.h"
#include "vgfx.h"
#include "fix16.h"

int v99x8_current_fd = -1;

void wait_for_vretrace(void) {
    while (!(v99x8_status_register_2_read() & V99X8_STATUS_REGISTER_2_VR)) {
        // Loop
    }
}

void wait_for_not_vretrace(void) {
    while (v99x8_status_register_2_read() & V99X8_STATUS_REGISTER_2_VR) {
        // Loop
    }
}

int main(int argc, char **argv, char **envp) {
    v99x8_current_fd = open("/dev/fb0", O_RDWR);
    if (v99x8_current_fd < 0) {
        err(EXIT_FAILURE, "Failed to open(/dev/v99x8)");
    }

    printf("Drawing field\n");

    v99x8_lmmv(0, 0, 256, 212, v99x8_rgb3x8_to_grb8(170, 170, 170), V99X8_ARGUMENT_MXD_VRAM | V99X8_ARGUMENT_DIY_DOWN | V99X8_ARGUMENT_DIX_RIGHT, V99X8_LOGICAL_OPERATION_IMP);

    uint32_t filled[32];
    for (unsigned row = 0; row < 32; ++row) {
        filled[row] = 0xFFFFFFFF;
    }
    vgfx_map_half_sphere_32(filled);
    uint8_t filled_sprite[4][4][8];
    vgfx_bitmap_to_spritemap_32(filled, filled_sprite);

    uint32_t dithered_filled[32];
    for (unsigned row = 0; row < 32; ++row) {
        dithered_filled[row] = row & 0x1 ? 0x55555555 : 0xAAAAAAAA;
        dithered_filled[row] &= filled[row];
    }
    uint8_t dithered_filled_sprite[4][4][8];
    vgfx_bitmap_to_spritemap_32(dithered_filled, dithered_filled_sprite);

    // Move all sprites in both pages off-screen
    for (int sprite = 0; sprite < 32; ++sprite) {
        v99x8_sm2_sprite_attribute_write(0xFA00, sprite, 216, 0, 0);
        v99x8_sm2_sprite_attribute_write(0x1FA00, sprite, 216, 0, 0);
    }

    uint8_t red[4][16];
    uint8_t white[4][16];
    uint8_t grey[4][16];
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 16; ++j) {
            red[i][j] = 0xA;
            white[i][j] = 0xF;
            grey[i][j] = 0x7;
        }
    }
    v99x8_sm2_sprite_color_write_4(0xFA00, 0, true, white);
    v99x8_sm2_sprite_color_write_4(0xFA00, 4, true, red);
    v99x8_sm2_sprite_color_write_4(0xFA00, 8, true, grey);
    v99x8_sm2_sprite_color_write_4(0x1FA00, 0, true, white);
    v99x8_sm2_sprite_color_write_4(0x1FA00, 4, true, red);
    v99x8_sm2_sprite_color_write_4(0x1FA00, 8, true, grey);

    fix16_unit_64 pos_x = 0;
    fix16_unit_64 pos_y = 100 * fix16_unit_64_one;

    fix16_unit_64 vel_x = 3 * fix16_unit_64_one;
    fix16_unit_64 vel_y = 0;

    fix16_unit_64 acc_x = 0;
    fix16_unit_64 acc_y = 1 * fix16_unit_64_one;

    const fix16_unit_64 wall_xn = 28 * fix16_unit_64_one;
    const fix16_unit_64 wall_xp = 228 * fix16_unit_64_one;
    const fix16_unit_64 wall_yn = 16 * fix16_unit_64_one;
    const fix16_unit_64 wall_yp = 196 * fix16_unit_64_one;

    const fix16_unit_64 sprite_size = 16 * fix16_unit_64_one;
    const fix16_unit_64 object_size = sprite_size * 2;

    long dtime = 1;

    bool wall_xn_bounces = true;
    bool wall_xp_bounces = true;
    bool wall_yn_bounces = false;
    bool wall_yp_bounces = true;

    fix16_unit_64 front_z = 60;
    fix16_unit_64 back_z = 80;

    for (fix16_unit_64 this_y = wall_yn; this_y <= wall_yp; this_y += 10 * fix16_unit_64_one) {
        struct vgfx_line line = {
            { wall_xn, this_y,  back_z},
            { wall_xp, this_y,  back_z}
        };
        vgfx_line_draw(&line, v99x8_rgb3x8_to_grb8(170, 0, 170));
    }
    for (long this_x = wall_xn; this_x <= wall_xp; this_x += 10 * fix16_unit_64_one) {
        struct vgfx_line line = {
            { this_x, wall_yn,  back_z},
            { this_x, wall_yp,  back_z}
        };
        vgfx_line_draw(&line, v99x8_rgb3x8_to_grb8(170, 0, 170));
    }
    for (long this_z = front_z; this_z < back_z; this_z += 4) {
        struct vgfx_line line = {
            { wall_xn, wall_yp, this_z},
            { wall_xp, wall_yp, this_z}
        };
        vgfx_line_draw(&line, v99x8_rgb3x8_to_grb8(170, 0, 170));
    }
    for (long this_x = wall_xn; this_x <= wall_xp; this_x += 10 * fix16_unit_64_one) {
        struct vgfx_line line = {
            { this_x, wall_yp, front_z},
            { this_x, wall_yp, back_z}
        };
        vgfx_line_draw(&line, v99x8_rgb3x8_to_grb8(170, 0, 170));
    }

    v99x8_sm2_sprite_pattern_generator_write_16(0xF000, 0, filled_sprite);
    v99x8_sm2_sprite_pattern_generator_write_16(0xF000, 32, dithered_filled_sprite);
    v99x8_sm2_sprite_pattern_generator_write_16(0x1F000, 0, filled_sprite);
    v99x8_sm2_sprite_pattern_generator_write_16(0x1F000, 32, dithered_filled_sprite);

    printf("Starting animation\nPress any key to exit\n");

    unsigned short rotation = 0;
    bool page2 = false;
    for (unsigned long time = 0; ; time += dtime) {
        if (getchar() != EOF)
            break;
        clearerr(stdin);

        if (pos_x <= wall_xn) {
            pos_x = wall_xn_bounces * (wall_xn - pos_x) + wall_xn + fix16_unit_64_one;
            vel_x = wall_xn_bounces * (-vel_x);
        }
        if (pos_x + object_size  >= wall_xp) {
            pos_x = wall_xp_bounces * (wall_xp - pos_x - object_size) + wall_xp - fix16_unit_64_one - object_size;
            vel_x = wall_xp_bounces * (-vel_x);
        }
        if (pos_y <= wall_yn) {
            pos_y = wall_yn_bounces * (wall_yn - pos_y) + wall_yn + fix16_unit_64_one;
            vel_y = wall_yn_bounces * (-vel_y);
        }
        if (pos_y + object_size >= wall_yp) {
            pos_y = wall_yp_bounces * (wall_yp - pos_y - object_size) + wall_yp - fix16_unit_64_one - object_size;
            vel_y = wall_yp_bounces * (-vel_y);
        }

        if (time % 4 == 0) {
            if (vel_x > 0) {
                rotation -= 1;
            } else if (vel_x < 0) {
                rotation += 1;
            }
            rotation %= 8;
        }
        uint32_t bars[32];
        for (unsigned row = 0; row < 32; ++row) {
            bars[row] = (0xF0F0F0F0F0 >> rotation) ^ (row & 0x8 ? 0xFFFFFFFF : 0);
        }
        vgfx_map_half_sphere_32(bars);
        uint8_t bars_sprite[4][4][8];
        vgfx_bitmap_to_spritemap_32(bars, bars_sprite);

        uint8_t sprite_pos_x = pos_x / fix16_unit_64_one;
        uint8_t sprite_pos_y = pos_y / fix16_unit_64_one;
        uint8_t shadow_pos_x = sprite_pos_x + 10;
        uint8_t shadow_pos_y = sprite_pos_y + 0;

        v99x8_sm2_sprite_attribute_write_4(page2 ? 0x1FA00 : 0xFA00, 0, sprite_pos_y, sprite_pos_x, 16, true);
        v99x8_sm2_sprite_attribute_write_4(page2 ? 0x1FA00 : 0xFA00, 4, sprite_pos_y, sprite_pos_x, 0, true);
        v99x8_sm2_sprite_attribute_write_4(page2 ? 0x1FA00 : 0xFA00, 8, shadow_pos_y, shadow_pos_x, 32, true);

        v99x8_sm2_sprite_pattern_generator_write_16(page2 ? 0x1F000 : 0xF000, 16, bars_sprite);

        wait_for_not_vretrace();
        wait_for_vretrace();

        v99x8_sprite_pattern_generator_table_write(((page2 ? 0x1F000 : 0xF000) >> 11) | 0x00);
        v99x8_sprite_attribute_table_write(((page2 ? 0x1FA00 : 0xFA00) >> 7) | 0x03);

        pos_x += vel_x * dtime / 2;
        pos_y += vel_y * dtime / 2;
        vel_x += acc_x * dtime;
        vel_y += acc_y * dtime;
        pos_x += vel_x * dtime / 2;
        pos_y += vel_y * dtime / 2;

        page2 = !page2;
    }

    close(v99x8_current_fd);

    return EXIT_SUCCESS;
}
