    .section .tbss, "T"
.tls_common _PDCLIB_errno, 4, 2

    .text

    .globl  __m68k_read_tp
    .type   __m68k_read_tp, @function
__m68k_read_tp:
    move.l  #_PDCLIB_errno,%a0
    rts
