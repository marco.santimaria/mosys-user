#include <sys/types.h>
#include <sys/socket.h>

#include <sys/syscall.h>

int socketpair(int domain, int type, int protocol, int sv[2]) {
    return syscall(SYS_socketpair, domain, type, protocol, sv);
}
