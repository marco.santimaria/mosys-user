#include <sys/types.h>
#include <sys/socket.h>

#include <sys/syscall.h>

int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
    return syscall(SYS_bind, sockfd, addr, addrlen);
}
