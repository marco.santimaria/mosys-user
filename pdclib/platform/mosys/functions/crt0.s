    .text

    .globl  _start
    .type   _start, @function
_start:
    | Push envp, argv, argc
    move.l  %a1,-(%sp)
    move.l  %a0,-(%sp)
    move.l  %d0,-(%sp)

    | Store envp to environ
    move.l  %a1,(environ)

    | Store argv[0] to __progname
    move.l  (0,%a0),(__progname)

    | Call main()
    jsr     main
    add.l   #12,%sp

    | Push main return value and call exit()
    move.l  %d0,-(%sp)
    jsr     exit
    add.l   #4,%sp
