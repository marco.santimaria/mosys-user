#include <spawn.h>

#include <errno.h>
#include <limits.h>
#include <sched.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct spawn_args {
    const char *path;
    bool path_is_absolute;
    const posix_spawn_file_actions_t *file_actions;
    const posix_spawnattr_t *attrp;
    char *const *argv;
    char *const *envp;
};

static int spawn_pre_exec(void *arg);
static int spawn_exec(struct spawn_args *args);

static int spawn_pre_exec(void *arg) {
    struct spawn_args *args = arg;

    // TODO: Perform housekeeping actions

    return spawn_exec(args);
}

static int spawn_exec(struct spawn_args *args) {
    if (args->path_is_absolute) {
        int exec_res = execve(args->path, args->argv, args->envp);
    } else {
        // Available length for path prefix is PATH_MAX minus NUL, slash, and file name length
        const size_t filename_len = strlen(args->path);
        if (filename_len >= PATH_MAX) {
            return 127; // ENAMETOOLONG
        }
        const size_t len_for_prefix_path = PATH_MAX - 2 - filename_len;

        // Make a copy of the PATH environment variable
        const char *path_env = getenv("PATH");
        if (!path_env) {
            return 127; // ENOENT;
        }
        const size_t path_len = strlen(path_env);
        char *const filepath = malloc(PATH_MAX + path_len + 1); // Combined buffer for filepath and path_dup
        if (!filepath) {
            return 127; // ENOMEM;
        }
        char *const path_dup = filepath + PATH_MAX;
        memcpy(path_dup, path_env, path_len + 1);

        char *path_next_entry = path_dup;
        do {
            // This path entry starts at the current position in PATH
            char *const path_entry = path_next_entry;

            // Find the end of this path entry and add a NUL there
            char *const path_entry_end = strchr(path_next_entry, ':');
            if (path_entry_end) {
                path_entry_end[0] = '\0';
                path_next_entry = path_entry_end + 1;
            } else {
                path_next_entry = NULL;
            }

            // Skip to the next entry if this one is too long
            size_t path_entry_len = strlen(path_entry);
            if (path_entry_len > len_for_prefix_path) {
                continue;
            }

            // Copy parts of path to filepath
            memcpy(&filepath[0], path_entry, path_entry_len);
            filepath[path_entry_len] = '/';
            memcpy(&filepath[path_entry_len + 1], args->path, filename_len);
            filepath[path_entry_len + 1 + filename_len] = '\0';

            // Try running with this path prefix entry
            execve(filepath, args->argv, args->envp);
        } while (path_next_entry);

        free(filepath);
    }

    return 127; // Specified return value for failing
}

int posix_spawn_do_clone(pid_t *pid, const struct spawn_args *args) {
    int clone_result = clone(spawn_pre_exec, NULL, CLONE_VM | CLONE_VFORK, args);

    if (clone_result >= 0) {
        if (pid) {
            *pid = clone_result;
        }
        return 0;
    } else {
        return clone_result;
    }
}

int posix_spawn(pid_t *pid, const char *path,
                const posix_spawn_file_actions_t *file_actions,
                const posix_spawnattr_t *attrp,
                char *const argv[], char *const envp[]) {
    struct spawn_args args = {
        path,
        true,
        file_actions,
        attrp,
        argv,
        envp
    };

    return posix_spawn_do_clone(pid, &args);
}

int posix_spawnp(pid_t *pid, const char *file,
                 const posix_spawn_file_actions_t *file_actions,
                 const posix_spawnattr_t *attrp,
                 char *const argv[], char *const envp[]) {
    struct spawn_args args = {
        file,
        strchr(file, '/'),
        file_actions,
        attrp,
        argv,
        envp
    };

    return posix_spawn_do_clone(pid, &args);
}
