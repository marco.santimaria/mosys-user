    .text

    .globl  syscall
    .type   syscall, @function
| long syscall(long number, ...)
syscall:
    link    %fp,#0

    | Save syscall-used non-volatile registers
    movem.l %d2-%d5,-(%sp)

    | Load function arguments to %d0-%d5/%a0
    movea.l %fp,%a1
    addq.l  #8,%a1   | Skip saved frame pointer and return address in frame
    movem.l (%a1)+,%d0-%d5/%a0

    | Trap to kernel
    trap    #0

    | If there is no error, skip storing errno
    tst.l   %d0
    bpl     0f

    | Get errno pointer to %a0
    move.l  %d0,%d2
    jsr     _PDCLIB_errno_func
    move.l  %d0,%a0
    move.l  %d2,%d0

    | Set errno to -%d0 and set %d0 to -1 if %d0
    neg.l   %d0
    move.l  %d0,(%a0)   | size must match sizeof(errno)
    moveq.l #-1,%d0

0:
    | Restore saved non-volatile registers
    movem.l (%sp)+,%d2-%d5

    unlk    %fp
    rts
