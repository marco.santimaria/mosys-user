#include <unistd.h>

#include <sys/syscall.h>

_Noreturn void _exit(int status) {
    syscall(SYS__exit, status);
    __builtin_unreachable();
}
