#include <string.h>

char **environ;

char **__environ_find_by_name(const char *name) {
    size_t name_len = strlen(name);
    for (char **this_var_ptr = environ; *this_var_ptr; ++this_var_ptr) {
        char *this_var = *this_var_ptr;
        if (strlen(this_var) < name_len + 1) {
            continue;
        }
        if (strncmp(this_var, name, name_len) == 0 && this_var[name_len] == '=') {
            return this_var_ptr;
        }
    }
    return NULL;
}
