#include <sched.h>

#include <stdlib.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include <stdio.h>

// TODO: The fprintf below does nothing, but it magically
//       fixes a crash that occurs when returning from fn

int clone(int (*fn)(void *), void *stack, int flags, void *arg, ...
          /* pid_t *parent_tid, void *tls, pid_t *child_tid */) {
    int status = syscall(SYS_clone, flags, stack);
    if (status != 0) {
        return status;
    }

    int fn_exit = fn(arg);
    fprintf(stderr, "");

    exit(fn_exit);
}
