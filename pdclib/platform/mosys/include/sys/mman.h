#ifndef _INCLUDE_SYS_MMAN_H
#define _INCLUDE_SYS_MMAN_H

#define PROT_EXEC   0x04
#define PROT_NONE   0x00
#define PROT_READ   0x01
#define PROT_WRITE  0x02

#define MAP_FIXED       0x01
#define MAP_PRIVATE     0x02
#define MAP_SHARED      0x04
#define MAP_ANONYMOUS   0x08
#define MAP_ANON        MAP_ANONYMOUS

#define MAP_FAILED ((void *) -1)

#ifndef _MODE_T
#define _MODE_T
typedef unsigned short mode_t;
#endif

#ifndef _OFF_T
#define _OFF_T
typedef long off_t;
#endif

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned long size_t;
#endif

void *mmap(void *addr, size_t length, int prot, int flags,
           int fd, off_t offset);
int munmap(void *addr, size_t length);

#endif
