#ifndef _INCLUDE_SYS_SYSCALL_H
#define _INCLUDE_SYS_SYSCALL_H

#define SYS_succeed     0
#define SYS_fail        1
#define SYS_getpid      2
#define SYS_getppid     3
#define SYS_open        4
#define SYS_close       5
#define SYS_read        6
#define SYS_write       7
#define SYS_nanosleep   8
#define SYS_clone       9
#define SYS_execve      10
#define SYS__exit       11
#define SYS_waitpid     12
#define SYS_getdents    13
#define SYS_ioctl       14
#define SYS_lseek       15
#define SYS_socket      16
#define SYS_socketpair  17
#define SYS_bind        18
#define SYS_recvfrom    19
#define SYS_sendto      20
#define SYS_accept      21
#define SYS_connect     22
#define SYS_mount       23
#define SYS_mmap        24
#define SYS_munmap      25
#define SYS_pread       26
#define SYS_pwrite      27
#define SYS_chdir       28
#define SYS_fchdir      29
#define SYS_reboot      30
#define SYS_unlink      10000

#define SYS_do_trace    0x80000000

#include <dirent.h>

// TODO: Remove, fix this?
#include <sys/types.h>
#include <unistd.h>

static ssize_t getdents(int fd, struct dirent *drnt, size_t count) {
    return syscall(SYS_getdents, fd, drnt, count);
}

#endif
