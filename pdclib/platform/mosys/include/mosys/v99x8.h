#ifndef _INCLUDE_MOSYS_V99X8_H
#define _INCLUDE_MOSYS_V99X8_H

#include <asm/ioctl.h>
#include <stdint.h>

#define V99X8_IOC_MAGIC 'v'

struct v99x8_ioc_control {
    unsigned char cr;
    uint8_t value;
};

struct v99x8_ioc_palette {
    unsigned char pr;
    uint16_t value;
};

struct v99x8_ioc_status {
    unsigned char sr;
    uint8_t value;
};

struct v99x8_ioc_command {
    uint16_t source_x;
    uint16_t source_y;
    uint16_t destination_x;
    uint16_t destination_y;
    uint16_t number_of_dots_x;
    uint16_t number_of_dots_y;
    uint8_t color;
    uint8_t argument;
    uint8_t command;
    const uint8_t *data_in;
    uint8_t *data_out;

    int16_t srch_result;
    uint8_t point_result;
};

#define V99X8_IOC_CONTROL_W _IOW(V99X8_IOC_MAGIC, 0, struct v99x8_ioc_control)
#define V99X8_IOC_PALETTE_W _IOW(V99X8_IOC_MAGIC, 1, struct v99x8_ioc_palette)
#define V99X8_IOC_STATUS_R  _IOR(V99X8_IOC_MAGIC, 2, struct v99x8_ioc_status)

// Commands
#define V99X8_IOC_HMMC      _IOW(V99X8_IOC_MAGIC, 0x1F, struct v99x8_ioc_command)
#define V99X8_IOC_YMMM      _IOWR(V99X8_IOC_MAGIC, 0x1E, struct v99x8_ioc_command)
#define V99X8_IOC_HMMM      _IOWR(V99X8_IOC_MAGIC, 0x1D, struct v99x8_ioc_command)
#define V99X8_IOC_HMMV      _IOW(V99X8_IOC_MAGIC, 0x1C, struct v99x8_ioc_command)
#define V99X8_IOC_LMMC      _IOW(V99X8_IOC_MAGIC, 0x1B, struct v99x8_ioc_command)
#define V99X8_IOC_LMCM      _IOR(V99X8_IOC_MAGIC, 0x1A, struct v99x8_ioc_command)
#define V99X8_IOC_LMMM      _IOWR(V99X8_IOC_MAGIC, 0x19, struct v99x8_ioc_command)
#define V99X8_IOC_LMMV      _IOW(V99X8_IOC_MAGIC, 0x18, struct v99x8_ioc_command)
#define V99X8_IOC_LINE      _IOW(V99X8_IOC_MAGIC, 0x17, struct v99x8_ioc_command)
#define V99X8_IOC_SRCH      _IOR(V99X8_IOC_MAGIC, 0x16, struct v99x8_ioc_command)
#define V99X8_IOC_PSET      _IOW(V99X8_IOC_MAGIC, 0x15, struct v99x8_ioc_command)
#define V99X8_IOC_POINT     _IOR(V99X8_IOC_MAGIC, 0x14, struct v99x8_ioc_command)
#define V99X8_IOC_STOP      _IO(V99X8_IOC_MAGIC, 0x10)

#endif
