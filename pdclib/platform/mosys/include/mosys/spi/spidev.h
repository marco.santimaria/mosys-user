#ifndef _INCLUDE_MOSYS_SPI_SPIDEV_H
#define _INCLUDE_MOSYS_SPI_SPIDEV_H

// This uses the same macros as Linux with the same values, for compatibility

#include <asm/ioctl.h>
#include <stdint.h>

#define SPI_CPHA        0x0001
#define SPI_CPOL        0x0002
// #define SPI_CS_HIGH     0x0004
// #define SPI_LSB_FIRST   0x0008
// #define SPI_3WIRE       0x0010
// #define SPI_LOOP        0x0020
// #define SPI_NO_CS       0x0040
// #define SPI_READY       0x0080
// #define SPI_TX_DUAL     0x0100
// #define SPI_TX_QUAD     0x0200
// #define SPI_RX_DUAL     0x0400
// #define SPI_RX_QUAD     0x0800
// #define SPI_CS_WORD     0x1000
// #define SPI_TX_OCTAL    0x2000
// #define SPI_RX_OCTAL    0x4000
// #define SPI_3WIRE_HIZ   0x8000

#define SPI_MODE_0  (0          | 0         )
#define SPI_MODE_1  (0          | SPI_CPHA  )
#define SPI_MODE_2  (SPI_CPOL   | 0         )
#define SPI_MODE_3  (SPI_CPOL   | SPI_CPHA  )

#define SPI_IOC_MAGIC 'k'

struct spi_ioc_transfer {
    void *tx_buf;
    void *rx_buf;

    size_t len;
    unsigned long speed_hz;

    unsigned short delay_usecs;
    unsigned char bits_per_word;
    bool cs_change;
    unsigned char word_delay_usecs;
};

#define SPI_MSGSIZE(N) ((N) * sizeof(struct spi_ioc_transfer))
#define SPI_IOC_MESSAGE(N) _IOW(SPI_IOC_MAGIC, 0, char[SPI_MSGSIZE(N)])

// 8-bit-limited SPI mode
#define SPI_IOC_RD_MODE             _IOR(SPI_IOC_MAGIC, 1, uint8_t)
#define SPI_IOC_WR_MODE             _IOW(SPI_IOC_MAGIC, 1, uint8_t)

// SPI bit justification
#define SPI_IOC_RD_LSB_FIRST        _IOR(SPI_IOC_MAGIC, 2, uint8_t)
#define SPI_IOC_WR_LSB_FIRST        _IOW(SPI_IOC_MAGIC, 2, uint8_t)

// SPI word length in bits
#define SPI_IOC_RD_BITS_PER_WORD    _IOR(SPI_IOC_MAGIC, 3, uint8_t)
#define SPI_IOC_WR_BITS_PER_WORD    _IOW(SPI_IOC_MAGIC, 3, uint8_t)

// Default max speed in Hz
#define SPI_IOC_RD_MAX_SPEED_HZ     _IOR(SPI_IOC_MAGIC, 4, uint32_t)
#define SPI_IOC_WR_MAX_SPEED_HZ     _IOW(SPI_IOC_MAGIC, 4, uint32_t)

// Full SPI mode
#define SPI_IOC_RD_MODE32           _IOR(SPI_IOC_MAGIC, 5, uint32_t)
#define SPI_IOC_WR_MODE32           _IOW(SPI_IOC_MAGIC, 5, uint32_t)

#endif
