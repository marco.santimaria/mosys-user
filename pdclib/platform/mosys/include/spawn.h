#ifndef _INCLUDE_SPAWN_H
#define _INCLUDE_SPAWN_H

#include <sys/types.h>

typedef void posix_spawn_file_actions_t;
typedef void posix_spawnattr_t;

int posix_spawn(pid_t *pid, const char *path,
                const posix_spawn_file_actions_t *file_actions,
                const posix_spawnattr_t *attrp,
                char *const argv[], char *const envp[]);
int posix_spawnp(pid_t *pid, const char *file,
                const posix_spawn_file_actions_t *file_actions,
                const posix_spawnattr_t *attrp,
                char *const argv[], char *const envp[]);

#endif
