#ifndef _INCLUDE_UNISTD_H
#define _INCLUDE_UNISTD_H

#include <stddef.h>
#include <sys/types.h>

#ifndef STDERR_FILENO
#define STDERR_FILENO 2
#endif
#ifndef STDIN_FILENO
#define STDIN_FILENO 0
#endif
#ifndef STDOUT_FILENO
#define STDOUT_FILENO 1
#endif

#ifndef SEEK_CUR
#define SEEK_CUR 0
#endif
#ifndef SEEK_END
#define SEEK_END 1
#endif
#ifndef SEEK_SET
#define SEEK_SET 2
#endif

int chdir(const char *path);
int close(int fd);
_Noreturn void _exit(int status);
int execve(const char *pathname, char *const argv[],
           char *const envp[]);
int fchdir(int fd);
pid_t getpid(void);
pid_t getppid(void);
off_t lseek(int fd, off_t offset, int whence);
ssize_t pread(int fd, void *buf, size_t count, off_t offset);
ssize_t pwrite(int fd, const void *buf, size_t count, off_t offset);
ssize_t read(int fd, void *buf, size_t count);
unsigned sleep(unsigned seconds);
long syscall(long number, ...);
ssize_t write(int fd, const void *buf, size_t count);

#endif
